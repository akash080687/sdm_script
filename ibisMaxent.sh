export $(egrep -v '^#' /var/www/html/sdm/public/.env | xargs)
if [ "${11}" = "rerun" ];then
   mv $2/outputs/*.html $2/outputs/index.html
   bioClims=`/usr/bin/php $4/parseHtml.php $2/outputs/index.html`
   rm -rf $2/*
fi

pgPassword=$IBIS_PASSWORD
pgHost=$IBIS_HOST
pgUser=$IBIS_USERNAME
pgDbName=$IBIS_DATABASE

echo "SDM started on `date +%x` at `date +%r`" >> $3/sdm.log
echo "Running analysis.." >> $3/sdm.log

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set status = 'Running' WHERE id = "$9
python  $4/analyze.py $1 $2/
ogr2ogr $2/"computed_shape" $2/"computed_shape.vrt" >> $2/"computed_shape.log"

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = 5 WHERE id = "$9

python  $4/create_poly.py $2/
python  $4/bufferAdd.py $2/"taxa_occurence.shp"

cp -f  $4/"taxa_occurence.shp_buffer.prj" $2/"taxa_occurence.shp_buffer.prj"

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = 10 WHERE id = "$9

echo "Buffer added & area computed" >> $3/sdm.log
echo "BioClim clipping started...." >> $3/sdm.log

mkdir $2/"clipped_bio"

if [ "${11}" = "rerun" ]; then   
	IFS=', ' read -r -a array <<< $bioClims
else
	IFS=', ' read -r -a array <<< "${10}"
fi

for i in "${array[@]}"
do
  gdalwarp -multi -q -cutline $2/"taxa_occurence.shp_buffer.shp" -tr 0.0083333333 0.0083333333 -of GTiff $5/"bio"$i"_isr.asc" $2/"clipped_bio/bio_"$i".tif"
  gdal_translate -of AAIGrid $2/"clipped_bio/bio_"$i".tif" $2/"clipped_bio/bio_"$i".asc"
done

if [ ${11} == 'expert' ]; then
  for filename in $3/../fileUploaded/supporting/*; do
    supFile=$(basename ${filename%.*})
    gdalwarp -multi -q -cutline $2/"taxa_occurence.shp_buffer.shp" -tr 0.0083333333 0.0083333333 -of GTiff "$5/"$supFile".asc" $2/"clipped_bio/"$supFile".tif"
    gdal_translate -of AAIGrid $2/"clipped_bio/"$supFile".tif" $2/"clipped_bio/"$supFile".asc"
  done
fi

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = 40 WHERE id = "$9

mkdir $2/"outputs"
echo "BioClim clipping completed...."
echo "BioClim clipping completed...." >> $3/sdm.log
echo "Maxent running ...."
echo "Maxent running ...." >> $3/sdm.log

/usr/bin/java -jar  $4/maxent.jar -z nowarnings noprefixes  responsecurves jackknife outputdirectory=$2/"outputs" samplesfile=$1 environmentallayers=$2/"clipped_bio/" perspeciesresults writeplotdata appendtoresultsfile maximumiterations=10000 threads=4 "applythresholdrule=10 percentile training presence" redoifexists autorun
echo "Maxent run completed ...."
echo "Maxent run completed ...." >> $3/sdm.log

if [ "${11}" == "beginner" ];then
   /bin/bash "$4/"maxent_process.sh $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} rerun
fi

echo "Exporting layer in Geoserver"
echo "Exporting layer in Geoserver" >> $3/sdm.log

if [ ! `cat $2/"outputs/maxent.log" | grep -ci Exception` = 0 ]; then
	PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = -1, status = 'Error..!' WHERE id = "$9
	exit
fi

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = 65 WHERE id = "$9

taxaname=`cat $2/"taxa_name.txt" | sed -e 's/ /_/g'`

gdal_translate -of "GTiff" $2/"outputs/$taxaname".asc $2/"outputs/$taxaname".tif

gdal_calc.py -A $2/"outputs/$taxaname".tif --outfile=$2/"outputs/$taxaname"_refactor.tif --calc="A*100"

mkdir $2/"outputs/shapefile"

gdal_polygonize.py $2/"outputs/$taxaname"_refactor.tif -f "ESRI Shapefile"  $2/"outputs/shapefile/$taxaname"_temp.shp

ogr2ogr -t_srs EPSG:4326 -s_srs EPSG:4326 $2/"outputs/shapefile/$taxaname".shp $2/"outputs/shapefile/$taxaname"_temp.shp

mv $2/outputs/*.html $2/outputs/index.html

cp -rf $2/"outputs/shapefile" $6

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = 70 WHERE id = "$9

# postgresql 

## link batch ID
view_name=${taxaname,,}"`date +%d%M%Y%H%I%S`"
ogrinfo $2/"outputs/shapefile/$taxaname".shp -sql "ALTER TABLE "$taxaname" ADD COLUMN batch_id integer(3)"
ogrinfo $2/"outputs/shapefile/$taxaname".shp -dialect SQLite -sql "UPDATE "$taxaname" SET batch_id = "$9

rm -rf $2/"outputs/shapefile/$taxaname"_temp.shp
rm -rf $2/"outputs/shapefile/$taxaname"_temp.shx
rm -rf $2/"outputs/shapefile/$taxaname"_temp.dbf

shp2pgsql -I -s 4326 -a $2/"outputs/shapefile/$taxaname".shp public."${12}" |  PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c 'CREATE VIEW '"$view_name"' AS SELECT * FROM '"${12}"' WHERE batch_id = '"$9"

string=$(/usr/bin/curl -u $8 -XGET -H \"Accept: text/xml\" http://$7:8080/geoserver/rest/workspaces/sdm)

if [[ $string == *"No such workspace"* ]]; then
   /usr/bin/curl -u $8 -XPOST -H "Content-type: text/xml" -d "<workspace><name>sdm</name></workspace>" http://$7:8080/geoserver/rest/workspaces  
   curl -u $8 -XPOST -H 'Content-type: text/xml' -d "<style><name>sdm</name><filename>sdm.sld</filename></style>" http://$7:8080/geoserver/rest/styles
   curl -u $8 -XPUT -H 'Content-type: application/vnd.ogc.sld+xml' -d @$4/sdm.sld http://$7:8080/geoserver/rest/styles/sdm_styl
fi

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = 90 WHERE id = "$9

#create xml for datastore

echo "<dataStore><name>"$view_name"</name><connectionParameters><host>$pgHost</host><port>5432</port><database>$pgDbName</database><user>$pgUser</user><passwd>$pgPassword</passwd><dbtype>postgis</dbtype></connectionParameters></dataStore>" > $2/view_datastore_tmp.xml

# create store
curl -v -u $8 -XPOST -T $2/view_datastore_tmp.xml -H "Content-type: text/xml" http://$7:8080/geoserver/rest/workspaces/sdm/datastores
# add layer
curl -v -u $8 -XPOST -H "Content-type: text/xml" -d "<featureType><name>"$view_name"</name></featureType>" http://$7:8080/geoserver/rest/workspaces/sdm/datastores/$view_name/featuretypes
# attach sld to layer
curl -v -u $8 -XPUT -H 'Content-type: text/xml' -d '<layer><defaultStyle><name>sdm</name></defaultStyle></layer>' http://$7:8080/geoserver/rest/layers/sdm:$view_name

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set status = 'Completed',layer_name = '"$view_name"' WHERE id = "$9

rm -rf $2/"outputs/$taxaname".tif
rm -rf $2/"outputs/$taxaname"_refactor.tif
rm -rf $2/"clipped_bio"

rm -rf $2/view_datastore_tmp.xml
rm -rf $2/computed_shape*
rm -rf $2/taxa_name.txt
rm -rf $2/taxa_occurence*

chmod -R 777 $2

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "Update ibis_batch_master set process = 100 WHERE id = "$9

echo "Layers added to geoserver" >> $3/sdm.log

