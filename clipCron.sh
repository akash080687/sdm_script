#!/bin/bash
arr=(`find /var/www/html/sdm/public/sdm -name clip.sh`)
for i in "${arr[@]}"
do
        dir=$(dirname "${i}")
        if [ ! -f $dir/running.txt ];then
                touch $dir/running.txt
                sudo bash $i
                rm -rf $i
                rm -rf $dir/running.txt
                fNm=`cat $dir/species.txt`
                touch $dir/completed.txt
                cp -rf /home/sdm/maxent/main.qml $dir/$fNm.qml
                zip -jm $dir/$fNm.zip $dir/$fNm.*
		# Remove area of interest file
		# ls $dir | grep .shp | sed 's/.shp//g' | rm -rf $dir/`tail -1`.*
                rm $dir/species.txt
        fi
done

