#!/bin/bash

# $1 is csv file path
# $2 is output file path
# $3 is the user
# ------------------
# yet to configure
# ------------------
# $4 sdmhome
# $5 bioClim home
# $6 geodatadir
# $7 ipaddess
# $8 credentials geoserver

SDMHOME="/home/fes/sdm_scripts"
BIOHOME="/home/fes/bio"
GEODATADIR="/opt/tomcat/webapps/geoserver/data/data"
ipaddress="192.168.12.19:8080"
credentials="admin:geoserver"

if [ ! -d $2 ]; then
   mkdir -p $2
fi

echo "SDM started on `date +%x` at `date +%r`" >> $2/sdm.log
echo "Running analysis.." >> $2/sdm.log

python  $SDMHOME/analyze.py $1 $2
ogr2ogr $2"computed_shape" $2"computed_shape.vrt" >> $2"computed_shape.log"
python  $SDMHOME/create_poly.py $2 
python  $SDMHOME/bufferAdd.py $2"taxa_occurence.shp"
# WORLDCLIM_bio19_99999999_99999999.tif 

echo "Buffer added & area computed" >> $2/sdm.log
echo "BioClim clipping started...." >> $2/sdm.log

mkdir $2"clipped_bio"
#cp -rf "/home/fes/sdm_scripts/malabar_test_clip/clipped_bio/" $2

for i in {1..19..1}
do
  gdalwarp -of VRT -cutline $2"taxa_occurence.shp_buffer.shp" -crop_to_cutline $BIOHOME"/WORLDCLIM_bio"$i"_99999999_99999999.tif" $2"clipped_bio/bio_"$i".vrt"
  gdal_translate -of AAIGrid $2"clipped_bio/bio_"$i".vrt" $2"clipped_bio/bio_"$i".asc"
done

 
mkdir $2"outputs"

echo "BioClim clipping completed...." >> $2/sdm.log

echo "Maxent running ...." >> $2/sdm.log

/usr/bin/java -jar  $SDMHOME/maxent.jar -z nowarnings noprefixes  responsecurves jackknife outputdirectory=$2"outputs" samplesfile=$1 environmentallayers=$2"clipped_bio/" perspeciesresults writeplotdata appendtoresultsfile maximumiterations=10000 threads=4 "applythresholdrule=10 percentile training presence" redoifexists autorun

echo "Maxent run completed ...." >> $2/sdm.log
echo "Exporting layer in Geoserver" >> $2/sdm.log

taxaname=`cat $2"/taxa_name.txt" | sed -e 's/ /_/g'`
dirname=$taxaname"`date +%d%M%Y%H%I%S`"

echo $2"outputs/$dirname" >> /tmp/1.txt
#/home/fes/sdm_scripts/bird_data/output/outputs/Corvus_splendens30052017130135

mkdir $2"outputs/$dirname"

gdal_translate -of "GTiff" $2"outputs/$taxaname".asc $2"outputs/$taxaname".tif

gdal_calc.py -A $2"outputs/$taxaname".tif --outfile=$2"outputs/$taxaname"_refactor.tif --calc="A*100" 
gdal_polygonize.py $2"outputs/$taxaname"_refactor.tif -f "ESRI Shapefile"  $2"outputs/$dirname/$taxaname"_temp.shp

#ogr2ogr -f "ESRI Shapefile" $2"outputs/$dirname/$taxaname".shp $2"outputs/$dirname/$taxaname"_temp.shp -t_srs EPSG:4326 -skipfailures

ogr2ogr -t_srs EPSG:4326 -s_srs EPSG:4326 $2"outputs/$dirname/$taxaname".shp $2"outputs/$dirname/$taxaname"_temp.shp


rm -rf $2"outputs/$taxaname".tif
rm -rf $2"outputs/$taxaname"_refactor.tif
rm -rf $2"clipped_bio"

mv $2"outputs/$dirname/$taxaname.shp" $2"outputs/$dirname/$dirname.shp"
mv $2"outputs/$dirname/$taxaname.shx" $2"outputs/$dirname/$dirname.shx"
mv $2"outputs/$dirname/$taxaname.dbf" $2"outputs/$dirname/$dirname.dbf"
mv $2"outputs/$dirname/$taxaname.prj" $2"outputs/$dirname/$dirname.prj"

rm -rf $2"outputs/$dirname/$taxaname"_temp.shp
rm -rf $2"outputs/$dirname/$taxaname"_temp.shx
rm -rf $2"outputs/$dirname/$taxaname"_temp.dbf

cp -rf $2"outputs/$dirname" $GEODATADIR
################################################################################################
#/usr/bin/php /var/www/html/pa/explore/sdm/sdmGeoserverAutomation.php $dirname
string=$(/usr/bin/curl -u $credentials -XGET -H \"Accept: text/xml\" http://$ipaddress/geoserver/rest/workspaces/sdm)

if [[ $string == *"No such workspace"* ]]; then
   /usr/bin/curl -u $credentials -XPOST -H "Content-type: text/xml" -d "<workspace><name>sdm</name></workspace>" http://$ipaddress/geoserver/rest/workspaces  
   curl -u $credentials -XPOST -H 'Content-type: text/xml' -d "<style><name>sdm</name><filename>sdm.sld</filename></style>" http://$ipaddress/geoserver/rest/styles
   curl -u $credentials -XPUT -H 'Content-type: application/vnd.ogc.sld+xml' -d @/home/fes/sdm_scripts/sdm.sld http://$ipaddress/geoserver/rest/styles/sdm_styl
fi


/usr/bin/curl -u $credentials -XPUT -H "Content-type: text/plain" -d "file://$2"outputs/"$dirname/$dirname".shp http://$ipaddress/geoserver/rest/workspaces/sdm/datastores/$dirname/external.shp

curl -u $credentials -XPUT -H 'Content-type: text/xml' -d '<layer><defaultStyle><name>sdm</name></defaultStyle></layer>' http://$ipaddress/geoserver/rest/layers/sdm:$dirname

echo "Layers added to geoserver" >> $2/sdm.log

echo $dirname >> $SDMHOME/userUploads/$3/layers.log
