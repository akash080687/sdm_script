from osgeo import ogr
import os
import csv
import sys

ring = ogr.Geometry(ogr.wkbLinearRing)
firstLine = True
taxafile = sys.argv[1]
dirpath = sys.argv[2]
with open(taxafile, 'rb') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
	for row in spamreader:
		if firstLine:
			firstLine = False
			continue
		if not os.path.exists(dirpath+'taxa_name.txt'):
		 	taxa_name=open(dirpath+'taxa_name.txt','w')
			taxa_name.write(row[0])
			taxa_name.close()
		#print row[1]+" "+row[2]
		ring.AddPoint(float(row[1]),float(row[2]))
poly = ogr.Geometry(ogr.wkbPolygon)
poly.AddGeometry(ring)
wktFormat=poly.ExportToWkt()

if(os.path.exists(dirpath+'conputed_shape.csv')):
	os.remove(dirpath+'computed_shape.csv')
with open(dirpath+'computed_shape.csv', 'wb') as csvfile:
	spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"')
	spamwriter.writerow(['id','geom'])
	spamwriter.writerow(['0',wktFormat])

if not os.path.exists(dirpath+'computed_shape'):
	os.makedirs(dirpath+'computed_shape')
'''
ALternative VRT format

vrt_format='<OGRVRTDataSource>\
    <OGRVRTLayer name="computed_shape">\
       <SrcDataSource>'+dirpath+'computed_shape.csv</SrcDataSource>\
      <GeometryType>wkbLineString25D</GeometryType>\
 <LayerSRS>PROJCS["WGS_1984_Lambert_Conformal_Conic",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Lambert_Conformal_Conic"],PARAMETER["False_Easting",1000000.0],PARAMETER["False_Northing",1000000.0],PARAMETER["Central_Meridian",85.875],PARAMETER["Standard_Parallel_1",24.625],PARAMETER["Standard_Parallel_2",27.125],PARAMETER["Latitude_Of_Origin",25.8772525],UNIT["Meter",1.0]]</LayerSRS>\
 <GeometryField encoding="WKT" field="geom" > </GeometryField >\
     </OGRVRTLayer>\
</OGRVRTDataSource>'
'''
vrt_format='<OGRVRTDataSource>\
    <OGRVRTLayer name="computed_shape">\
       <SrcDataSource>'+dirpath+'computed_shape.csv</SrcDataSource>\
      <GeometryType>wkbLineString25D</GeometryType>\
 <LayerSRS>WGS84</LayerSRS>\
 <GeometryField encoding="WKT" field="geom" > </GeometryField >\
     </OGRVRTLayer>\
</OGRVRTDataSource>'

if(os.path.exists(dirpath+'computed_shape.vrt')):
        os.remove(dirpath+'computed_shape.vrt')

target=open(dirpath+'computed_shape.vrt','w')
target.write(vrt_format)
target.close()
