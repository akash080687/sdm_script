#!/bin/bash
export $(egrep -v '^#' /var/www/html/sdm/public/.env | xargs)
password=$PGL_PASSWORD
host=$PGL_HOST
user=$PGL_USERNAME
database=$PGL_DATABASE
port=$PGL_PORT
# check geoserver running
if [ ! `netstat -ntlp | grep -ci 8080` = 0 ];then
  echo "Geoserver is running."
else
	echo "Geoserver is not running"
	exit
fi

ca=$(gdalinfo --version)	
# print gdal version
if [[ $ca = *"1.11"* ]];then
  echo "GDAL is running....."
else
	echo "GDAL is not installed"
	exit
fi

# check postgres running
#if [ ! `netstat -ntlp | grep -ci 5432` = 0 ];then
#  echo "Postgres is running....."
#else
#	echo "Postgres is not installed"
#	exit
#fi

### All system checks finished
echo "System services running fine. Proceeding to SDM run."
echo "PGPASSWORD=$password psql -h $host -U $user -d $database -p $port -t -c "select scriptcommand from batch_master where status='Running' limit 1""
running=`PGPASSWORD=$password psql -h $host -U $user -d $database -p $port -t -c "select scriptcommand from batch_master where status='Running' limit 1"`
if [ -z "$running" ]; then
	in_queue=`PGPASSWORD=$password psql -h $host -U $user -d $database -p $port -t -c "select scriptcommand from batch_master where status='In queue' limit 1"`
	if [ ! -z "$in_queue" ];then
  		in_queue=$(echo ${in_queue} | sed -e 's/^ *//g;s/ *$//g')
  		bash $in_queue
	fi
fi
